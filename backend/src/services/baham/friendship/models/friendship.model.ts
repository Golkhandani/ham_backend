import { prop, arrayProp, pre, Typegoose, index } from '@typegoose/typegoose';
import { IsString, IsOptional } from 'class-validator';
import * as uuid from 'node-uuid';
import { SchemaOptions } from 'mongoose';
import { FriendshipStatus } from '../enums/friendshipStatus.enum';

export const friendshipSchemaOptions: SchemaOptions = {
    collection: 'ham_relation',
    timestamps: true,
    autoIndex: true,
};
@index({ source_id: 1, target_id: 1 }, { unique: true })
export class Friendship {
    @prop({
        default: () => uuid.v4(),
        index: true,
    })
    friendship_id: string;

    @prop({
        required: true,
        index: true,
        type: String,
    })
    source_id: string;
    @prop({
        required: true,
        index: true,
        type: String,
    })
    target_id: string;

    @prop({
        required: true,
        index: true,
        type: String,
        enum: FriendshipStatus,
    })
    status: FriendshipStatus;

    @prop({
        type: String,
    })
    message: string;

}
