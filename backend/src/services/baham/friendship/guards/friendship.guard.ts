
import { Injectable, CanActivate, ExecutionContext, ForbiddenException, UnauthorizedException, Inject, createParamDecorator } from '@nestjs/common';
import { Observable } from 'rxjs';
import { SetMetadata } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { User } from '@shared/models/users.model';
import { AuthGuard } from '@nestjs/passport';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthenticationProvider } from '@services/authentication/authentication.provider';
import { Request } from 'express';
import { FriendshipService } from '../friendship.provider';
import { UserInHeader } from '@shared/decorators';
export const targetReqPath = (reqPath: string) => {
    return SetMetadata('targetReqPath', reqPath);
};
export const targetIdPath = (idPath: string) => {
    return SetMetadata('targetIdPath', idPath);
};

export const friendshipFromHeader = createParamDecorator((data: string, req): User => {
    return data ? req.friendship && req.friendship[data] : req.friendship;
});
@Injectable()
/**
 * Must be after user guard
 */
export class FriendShipGuard implements CanActivate {
    constructor(
        @Inject(FriendshipService.name)
        private readonly friendshipService: FriendshipService,
        private readonly reflector: Reflector,
    ) {
    }
    async canActivate(context: ExecutionContext): Promise<boolean> {

        const request: Request = context.switchToHttp().getRequest();
        const user: UserInHeader = request.user;
        console.log('user', request.user);
        const user_id: string = user.user_id;
        const target_req_path = this.reflector.get<string>('targetReqPath', context.getHandler());
        const target_id_path = this.reflector.get<string>('targetIdPath', context.getHandler());

        const target_id = request[target_req_path][target_id_path];
        console.log('user_id', user_id);
        console.log('params', request.params);
        console.log('target_id', target_id);
        const { isValid, friendship } = await this.friendshipService.hasValidFriendship(user_id, target_id);
        (request as any).friendship = friendship;
        return true;
    }

}
