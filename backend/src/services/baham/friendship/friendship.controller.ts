import { Controller, Get, Post, Body, UseGuards, Put, Param, Delete, Query } from '@nestjs/common';
import { FriendshipService } from './friendship.provider';
import { UserFromHeader, UserInHeader } from '@shared/decorators';
import { CreateFriendshipDto } from './dtos/createFriendship.dto';
import { UserGuard } from '@shared/guards';
import { UpdateFriendshipDto } from './dtos/updateFriendship.dto';
import { FriendShipGuard, targetReqPath, targetIdPath } from './guards/friendship.guard';

@Controller(FriendshipController.path)
export class FriendshipController {
    public static path = 'friendships';
    constructor(private readonly friendshipService: FriendshipService) { }
    @targetReqPath('params')
    @targetIdPath('t_id')
    @UseGuards(FriendShipGuard)
    @Get('ping/:t_id')
    async pong() {
        return 'pong';
    }

    @UseGuards(UserGuard)
    @Post('')
    async create(
        @UserFromHeader() user: UserInHeader,
        @Body() friendshipObj: CreateFriendshipDto,
    ) {
        return {
            data: await this.friendshipService.createNewFriendship(user, friendshipObj),
        };
    }

    @UseGuards(UserGuard)
    @Put(':friendship_id')
    async update(
        @Body() friendshipObj: UpdateFriendshipDto,
        @Param('friendship_id') friendship_id: string,
    ) {
        return {
            data: await this.friendshipService.updateFriendship(friendship_id, friendshipObj),
        };
    }
    @UseGuards(UserGuard)
    @Delete(':friendship_id')
    async delete(
        @Param('friendship_id') friendship_id: string,
    ) {
        return {
            data: await this.friendshipService.deleteFriendship(friendship_id),
        };
    }

    @UseGuards(UserGuard)
    @Get('')
    async find(
        @UserFromHeader() user: UserInHeader,
        @Query('user_id') person2_id: string,
    ) {
        return {
            data: await this.friendshipService.findFriendship(user, person2_id),
        };
    }
}
