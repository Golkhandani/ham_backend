import { Module } from '@nestjs/common';
import { FriendshipController } from './friendship.controller';
import { FriendshipService } from './friendship.provider';
import { TypegooseModule } from 'nestjs-typegoose';
import { Friendship, friendshipSchemaOptions } from './models/friendship.model';

@Module({
    imports: [
        TypegooseModule.forFeature([{
            typegooseClass: Friendship,
            schemaOptions: friendshipSchemaOptions,
        }])],
    controllers: [FriendshipController],
    providers: [FriendshipService],
    exports: [FriendshipService],
})
export class FriendshipModule { }
