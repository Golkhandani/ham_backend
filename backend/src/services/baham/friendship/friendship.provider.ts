import { Injectable } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';
import { Friendship } from './models/friendship.model';
import { InjectModel } from 'nestjs-typegoose';
import { CreateFriendshipDto } from './dtos/createFriendship.dto';
import { UserInHeader } from '@shared/decorators';
import { FriendshipStatus } from './enums/friendshipStatus.enum';
import { UpdateFriendshipDto } from './dtos/updateFriendship.dto';

@Injectable()
export class FriendshipService {
    constructor(
        @InjectModel(Friendship) private readonly FriendshipModel: ReturnModelType<typeof Friendship>,
    ) { }

    public async createNewFriendship(user: UserInHeader, friendshipObj: CreateFriendshipDto) {
        const newFriendship = new Friendship();
        newFriendship.source_id = user.user_id;
        newFriendship.target_id = friendshipObj.target_id;
        newFriendship.message = friendshipObj.message;
        newFriendship.status = FriendshipStatus.PENDING;
        return await this.FriendshipModel.create(newFriendship);
    }

    public async updateFriendship(friendship_id: string, friendshipObj: UpdateFriendshipDto) {
        const updateFriendship = new Friendship();
        updateFriendship.status = friendshipObj.status;

        return await this.FriendshipModel.findOneAndUpdate(
            { friendship_id },
            { $set: updateFriendship },
            { new: true },
        );
    }

    public async deleteFriendship(friendship_id: string) {
        return await this.FriendshipModel.findOneAndDelete({ friendship_id });
    }

    public async findFriendship(user: UserInHeader, person2_id: string) {
        return await this.FriendshipModel.findOne({
            $or: [
                {
                    $and: [
                        { source_id: user.user_id },
                        { target_id: person2_id },
                    ],
                },
                {
                    $and: [
                        { source_id: person2_id },
                        { target_id: user.user_id },
                    ],
                },
            ],
        });
    }

    /**
     * FOR guard requirements
     * @param user_id
     * @param person2_id
     */
    public async hasValidFriendship(user_id: string, person2_id: string) {
        const friendship = await this.FriendshipModel.findOne({
            $or: [
                {
                    $and: [
                        { source_id: user_id },
                        { target_id: person2_id },
                    ],
                },
                {
                    $and: [
                        { source_id: person2_id },
                        { target_id: user_id },
                    ],
                },
            ],
        });
        if (friendship) {
            return {
                isValid: true,
                friendship,
            };
        } else {
            return {
                isValid: false,
                friendship,
            };
        }
    }

}
