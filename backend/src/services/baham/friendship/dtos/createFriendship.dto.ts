import { IsString, IsOptional, IsDefined } from 'class-validator';
export class CreateFriendshipDto {
    @IsDefined()
    target_id: string;
    @IsOptional()
    message?: string;
}

