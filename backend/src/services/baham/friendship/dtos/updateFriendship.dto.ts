import { IsDefined } from 'class-validator';
import { FriendshipStatus } from '../enums/friendshipStatus.enum';

export class UpdateFriendshipDto {
    @IsDefined()
    status: FriendshipStatus;
}
