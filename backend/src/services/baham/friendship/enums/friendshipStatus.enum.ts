export enum FriendshipStatus {
    PENDING = 'PENDING',
    BLOCK = 'BLOCK',
    ACCEPTED = 'ACCEPTED',
}
