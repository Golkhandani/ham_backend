export enum LocationOwnerType {
    USER = 'USER',
    VENUE = 'VENUE'
}
