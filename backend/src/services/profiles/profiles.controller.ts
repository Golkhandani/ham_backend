import {
    Controller,
    Get, Post, Put, Delete,
    Request,
    Body, Query,
    UploadedFile,
    UseGuards,
    UseInterceptors,
    CacheInterceptor,
    Param,

} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';

import { UserFromHeader, UserInHeader } from '@shared/decorators';
import { UserGuard as RoleGuard, Roles, Scopes } from '@shared/guards';
import { User, UserRoles } from '@shared/models';
import { IApi, IMulterFile } from '@shared/interfaces';

import { UsersProfileProvider } from '@services/profiles/profiles.provider';
import { UpdateUserDto } from '@services/profiles/dtos';

import { UserScopes } from '@services/authorization/models';
import { friendshipFromHeader, FriendShipGuard, targetReqPath, targetIdPath } from '@services/baham/friendship/guards/friendship.guard';

@Controller(UsersProfileController.path)
@ApiTags(UsersProfileController.path)
export class UsersProfileController {

    public static path = 'users';

    @Get('ping')
    getPing() { return 'pong'; }
    constructor(
        private readonly usersProfileProvider: UsersProfileProvider,
    ) { }

    //#region USER SELF PROFILE
    @Roles(UserRoles.USER)
    @Scopes(UserScopes.ME)
    @UseGuards(RoleGuard)
    @Get('self')
    async getProfile(@UserFromHeader() user: UserInHeader): Promise<IApi<User>> {
        return {
            data: await this.usersProfileProvider.getProfile(user),
        };
    }

    @Scopes(UserScopes.ME)
    @Roles(UserRoles.USER)
    @UseGuards(RoleGuard)
    @Put('self')
    async updateProfile(
        @UserFromHeader() user: UserInHeader,
        @Body() updates: UpdateUserDto,
    ): Promise<IApi<User>> {
        return {
            data: await this.usersProfileProvider.updateProfile(user, updates),
        };
    }

    @Scopes(UserScopes.ME)
    @UseGuards(RoleGuard)
    @Put('self/picture')
    @UseInterceptors(FileInterceptor('file'))
    async updateProfilePicture(
        @UserFromHeader() user: UserInHeader,
        @UploadedFile() file: IMulterFile): Promise<IApi<User>> {
        return {
            data: await this.usersProfileProvider.updateProfilePicture(user, file),
        };
    }
    @Scopes(UserScopes.ME)
    @UseGuards(RoleGuard)
    @Delete('self/picture')
    async deleteProfilePicture(
        @UserFromHeader() user: UserInHeader): Promise<IApi<User>> {
        return {
            data: await this.usersProfileProvider.deleteProfilePicture(user),
            message: 'user picture updated',
        };
    }
    //#endregion

    //#region GET OTHER PROFILE
    @targetReqPath('params')
    @targetIdPath('user_id')
    @UseGuards(FriendShipGuard)

    @Roles(UserRoles.USER)
    @UseGuards(RoleGuard)
    @Get(':user_id')
    async getOthersProfile(
        @friendshipFromHeader() friendship,
        @Param('user_id') user_id: string): Promise<IApi<User>> {
        return {
            data: await this.usersProfileProvider.getOthersProfile(user_id, friendship),
        };
    }
    //#endregion

    @Get('transactions')
    async testTransaction() {
        return await this.usersProfileProvider.transactionTest();
    }
}
