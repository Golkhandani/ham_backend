import { IsAlpha, NotEquals, Validate, IsOptional, ValidateNested } from 'class-validator';

import { UserAlreadyExist } from '@services/authentication/validators';
import { BankAccountDto } from '.';
import { GeoLocation } from '@shared/models/geoLocation.model';

export class UpdateUserDto {
    @IsOptional()
    public name: string;

    @IsOptional()
    public biography: string;

    @IsOptional()
    public dateOfBirth: Date; // Date;
    @IsOptional()
    public address: string;
    @IsOptional()
    public geoLocation: GeoLocation;
    @IsOptional()
    public favourites: Array<{ favourite_id: string }>;

    @IsOptional()
    public isPrivate: boolean;

}
