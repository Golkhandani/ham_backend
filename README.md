# Ba-Ham project 
## WIP: subtitle
###### WIP: description

## All links
|Name| Link |
| -- | ---- |
| Trello | https://trello.com/b/96nCc5nn/ham-project |
| XD design | https://xd.adobe.com/view/db84b3e2-568a-45b4-6fae-65067b736d69-adb5/ |
| User story | https://miro.com/app/board/o9J_kv4XKWY=/ |


## All contacts
|Name| E-mail |
| -- | ------ |
| Mr Yousefi | Mohammadaminyousefi71@gmail.com |
| Mr Salehi | meysam_salehi@outlook.com |


## Branches
|Name| Description |
| -- | ------ |
| master | Main code for production environment |
| staging | For stablity testing (weekly update) |
| dev | For development purposes |
| dev-{feature}-#{taskNumber} | For new features (they will be merged into dev branch) |



# Project structure
###### Based on Nest.js Folder structure
> baham logics folders will be added after a more detailed look

    .📦backend
    ├──📦src                      # Main Codes of Application
    |   ├──📂constants            # All the constants  
    |   ├──📂services             # Project Specific structure
    |   |   ├──📂authenticaton    # Authentication Logics
    |   |   ├──📂authorizaton     # Authorization Logics
    |   |   ├──📦baham            # Baham specific logics
    |   |   |   ├──friendship     # Baham friendship logic
    |   |   |   └── ...
    |   |   ├──📂notificaton      # Notification Logics
    |   |   ├──📂profiles         # Users Logics
    |   ├──📂shared               # Shared file like interceptors/helpers/logger/etc.
    |   ├────📜app.module.ts      # App starter module
    |   ├────📜main.ts            # App starter bootstrap
    ├──📂test                     # Test files (alternatively `spec`) 
    ├────📜package.json
    ├────📜Dockerfile 
    ├────📜tsconfig.json 
    ├────📜development.env        # enviroments file for local development 
    └── ...

# Project database Collections
###### We are in early stage so there will be another collections for persist data
| Collection | Description |
| - | - |
| Users | فهرست کاربران و پروفایل هاشون |
| Auth_phone_verifications | برای هندل کردن سیستم لاگین |
| Auth_refresh_tokens | برای هندل کردن سیستم رفرش jwt |
| Auth_user_scopes | برای هندل کردن سطوح مختلف دسترسی |
| Media_items | فهرست کامل همه فایل های چند رسانه ای برای تمامی کالکشن ها |
| Notification_messages | فهرست پیام های فرستاده شده از طریق نوتیفیکشن سیستم |
| Notification_players | روشهای ارتباطی با کاربران شامل ایدی پوش یا ... |
| Ham_venues | فهرست مکان ها |
| Ham_favourites | لیست علاقه مندی ها |
| Ham_histories | فهرست با هم بودن ها شامل امتیاز ها چگونگی بدست اوردن ها و نفرات درگیر در امتیاز |
| Ham_relations | فهرست روابط شامل درخواست های دوستی بلاک شده ها دوستی ها  |
| Chat_messages | پیام های داخل چت |
| Game_levels | فهرست لیست لول ها شامل شروع و پایان امتیاز ها  |
| Game_actions | ؟ |
| Money_requests | درخواست نقد کردن |
| Spend_requests | درخواست های مربوط به کارت های تخقیف و غیره |
| Locations | درصورتی که place ها و user ها بخوان با یک pagination بدست بیان بهتره locations جدا بشه از باقی داستان ها |

# Project endpoints list 
#### These are just endpoint more detail will be added to swagger files
`var baseUrl = api/v1/`
##### Authentication

|Endpoint|Description|
| - | - |
| POST `authentication/signup/phone` | - |
| POST `authentication/signin/phone` | - |
| POST `authentication/refresh` | - |


##### Users
|Endpoint|Description|
| - | - |
| GET `users/self` | - |
| PUT `users/self` | - |
| PUT `users/self/picture` | - |
| DELETE `users/self/picture` | - |
##### Ham_venues
|Endpoint|Description|
| - | - |
| ` _ ` | - |
##### Ham_favourites
|Endpoint|Description|
| - | - |
| ` _ ` | - |

##### Ham_histories
|Endpoint|Description|
| - | - |
| ` _ ` | - |

##### Ham_relations
|Endpoint|Description|
| - | - |
| ` _ ` | - |

##### Chat_messages
|Endpoint|Description|
| - | - |
| ` _ ` | - |
##### Game_levels
|Endpoint|Description|
| - | - |
| ` _ ` | - |
##### Game_actions
|Endpoint|Description|
| - | - |
| ` _ ` | - |
##### Money_requests
|Endpoint|Description|
| - | - |
| ` _ ` | - |
##### Spend_requests
|Endpoint|Description|
| - | - |
| ` _ ` | - |
##### Locations
|Endpoint|Description|
| - | - |
| ` _ ` | - |
